<?php
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class FormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name');
        $builder->add('email', EmailType::class);
        $builder->add(
            'ciudad',
            ChoiceType::class, [
                'choices' => [
                    'Madrid' => 'madrid',
                    'Barcelona' => 'barcelona',
                    'Granada' => 'granada',
                ]
            ]
        );
        $builder->add('privacy', CheckboxType::class, [
            'label'    => "Aceptas la <a href='#'>política de privacidad </a>",
            'label_html'=> true,
            'required' => true,
        ]);
    }
}
