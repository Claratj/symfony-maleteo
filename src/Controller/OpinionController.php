<?php


namespace App\Controller;


use App\Entity\Opinions;
use App\Form\FormOpinions;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class OpinionController extends AbstractController
{
    /**
    * @Route ("/newOpinion", name="newOpinion")
     */
    public function insertOpinion(Request $r, EntityManagerInterface $doctrine)
    {
        $OpinionForm= $this->createForm(FormOpinions::class);
        $OpinionForm->handleRequest($r);

        if($OpinionForm->isSubmitted() && $OpinionForm->isValid())
        {
            $opinion=$OpinionForm->getData();

            $newOpinion = new Opinions();

            $newOpinion->setAuthor($opinion["author"]);
            $newOpinion->setText($opinion["text"]);

            $doctrine->persist($newOpinion);
            $doctrine->flush($newOpinion);

            return $this->redirectToRoute("home");

        } else
        {
            return $this->render('Register/OpinionsForm.html.twig', ['OpinionForm' => $OpinionForm->createView()]);
        }
    }
}