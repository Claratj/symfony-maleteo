<?php


namespace App\Controller;

use App\Entity\Opinions;
use App\Form\FormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route ("/home", name="home")
     */
    public function home(EntityManagerInterface $doctrine, Request $request)
    {
        $form = $this->createForm(FormType::class);

        $form->handleRequest($request);  //hace que parezca que los datos están siendo procesados. Solo vale para metodos post.

        if($form->isSubmitted() && $form->isValid()) {
            // dd($form->getData());

           $data =$form->getData();


           $this->addFlash('success', 'Demo solicitada!');
        }
        $repository = $doctrine->getRepository(Opinions::class);
        $opinionsToShow = $repository->findAll();       //hemos recogido todas las opiniones de la base de datos en un array.
        shuffle($opinionsToShow);

        return $this->render('base.html.twig', [
            'sectionForm'=> $form->createView(),
            'opinions'=> $opinionsToShow,
        ]);


    }
}
